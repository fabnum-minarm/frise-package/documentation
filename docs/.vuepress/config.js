const { description } = require('../../package')

module.exports = {
  title: 'FRISE',
  base: "/frise-package/documentation/",
  dest: 'public',

  head: [
    ['meta', { name: 'theme-color', content: '#5d77ff' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  themeConfig: {
    repo: '',
    editLinks: false,
    docsDir: '',
    editLinkText: '',
    lastUpdated: false,
    nav: [
      {
        text: 'Documentation',
        link: '/documentation/',
      },
      {
        text: 'Example',
        link: '/example/'
      },
      {
        text: 'GitLab',
        link: 'https://gitlab.com/fabnum-minarm/frise-package/frise'
      }
    ],
    sidebar: {
      '/documentation/': [
        {
          title: 'Documentation',
          collapsable: false,
          children: [
            '',
            'getstarted',
            'props',
            'slots',
            'events',
          ]
        }
      ],
    }
  },

  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
  ]
}
