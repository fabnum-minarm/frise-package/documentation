# Slots

## #navigation
**Deal with time navigation.**

You can navigate through time with two actions :
- jump to a specific date with direct date selection
- move backward/forward to the adjescent time section

3 methods are available from Frise's navigation slot :
| Name                       | Action                                                                                        | Argument     | Argument's type |
| -------------------------- | --------------------------------------------------------------------------------------------- | ------------ | --------------- |
|**goForward()**             | update internal reference date of chart, adding timeConfig.totalUnit of timeConfig.baseUnit   | None         | /               |
|**goBackward()**            | update internal reference date of chart, removing timeConfig.totalUnit of timeConfig.baseUnit | None         | /               |
|**selectDate(selectedDate)**| update internal reference date of chart, with selectedDate                                    | selectedDate | Date            |

**Usage :**
```shell
  <template #navigation="{ goForwards, goBackwards, selectDate }">
    <div class="frise-navigation">
      <input type="date" v-model="selectedDate" @input="selectDate"/>
      <button @click.native="goBackwards">Previous</>
      <button @click.native="goForwards">Next</>
    </div>
  </template>
```

## #timeAxisUnit
**Display time axis' unit.**

In this slot, you can access the associated date of the unit, with the `date` prop, and customize display format and style for specific date conditions.

**Usage :**
```shell
  <template #timeAxisUnit="{ date }">
    <div>{{ date }}</div>
  </template>
```

## #rowHeader
**Display row header's information.**

Header's content passed to data property is directly and fully available in this slot, with the `header` prop.

**Usage :**
```shell
  <template #rowHeader="{ header }">
    <RowHeader :header="header" />
  </template>
```

## #gridCell
**Customize row's time sub-unit's style.**

In this slot, you can access the associated date of a sub-cell, with the `date` prop, and customize the style of sub-cells and in fine cells, for specific date conditions.

You can also access the `index` property, to get the index of the sub-cell among 0 and the styleConfig.gridSubdivisions value, in order to add conditionnal style according to the position of the sub-cell assiociated to the same date.

**Warning** : what you pass as a slot is the sub-cell element !

**Usage :**
```shell
  <template #gridCell="{ date, index }">
    <Cell :date="date" :index="index" />
  </template>
```

## #block
**Display block's information.**

Block's content passed to data property is directly and fully available in this slot, with the **block** prop.

**Usage :**
```shell
  <template #block="{ block }">
    <Block :block="block" />
  </template>
```