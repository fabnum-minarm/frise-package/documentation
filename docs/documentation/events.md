# Events
**FRISE will emit different events, that you can listen from your higher component.**

## @drop-item
A `drop-item` event is emitted when an item has been dragged and dropped, from one row to another.

**Warning** : Only vertical D&D is available : you can move a block from one row to another - without changing it's time data/position.

This event emits an Object containing the moved `blockId`, the `initialRowId`, and the `newRowId`.

**Example** :
```html
<template>
  <Frise :data="data" @drop-item="updateRows" />
</template>

<script>
import Frise from '@fabnumdef/frise';

export default {
  components: {
    Frise
  },
    
  data: () => ({
    data: [
      {
        id: 'RowId#1',
        header: {
          name: 'A380',
          type: 'Airplane'
        },
        blocks: [
          {
            id: 'BlockId#1',
            start: new Date(09/09/2021),
            end: new Date(09/10/2021),
          },
        ],
      },
    ]
  }),
    
  methods: {
    updateRows(blockId, initialRowId, newRowId) {
        /** Your logic to update rows on bar drop **/
    }
  }
}    
</script>
```

## @update-date-interval
An `update-date-interval` event is emitted when time navigation has been triggered.

This event emits an Object containing the updated `start` and `end` dates to display.

**Example** :
```html
<template>
  <Frise :data="data" @update-date-interval="updateStartingDate" />
</template>

<script>
import Frise from '@fabnundef/frise';

export default {
  components: {
    Frise
  },
    
  data: () => ({
    data: [
      {
        id: 'RowId#1',
        header: {
          name: 'A380',
          type: 'Airplane'
        },
        blocks: [
          {
            id: 'BlockId#1',
            start: new Date(09/09/2021),
            end: new Date(09/10/2021),
          },
        ],
      },
    ]
  }),
    
  methods: {
    updateStartingDate({ start, end }) {
      /** Your logic to update fetched data for this time interval **/
    }
  }
}    
</script>
```