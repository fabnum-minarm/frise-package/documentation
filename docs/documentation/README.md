# Introduction

**FRISE is a light package to help you build a simple and customizable gantt chart.**

**Features :**
- ⌛ Custom timeline duration and unit
- 🧭 Custom timeline navigation
- 📖 Custom UI with transparent slot API and exposed events
- 💥 Event collision management
- ✨ Vertical drag and drop
