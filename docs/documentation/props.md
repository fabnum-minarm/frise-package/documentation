# Props
**FRISE component can take 3 props as arguments.**

## data
**Pass data to display.**

**Type** :
An array of rows.

A row is an Object with the given minimal structure :
```shell
row = {
  id: (String),
  header: (Object),
  blocks: (Array of blocks),
}
```
Header's structure is free, you'll get this plain object to display it as a slot (checkout Slot's section).

A block should have this minimal structure :
```shell
block = {
  id: (String),
  start: (Date),
  end: (Date),
}
```

**Example** :
```shell
data = [
  {
    id: 'RowId#1',
    header: {
      name: 'A380',
      type: 'Airplane'
    },
    blocks: [
      {
        id: 'BlockId#1',
        start: new Date(09/09/2021),
        end: new Date(09/10/2021),
      },
      {
        id: 'BlockId#2',
        start: new Date(09/12/2021),
        end: new Date(09/14/2021),
      },
    ],
  },
]
```
**By default** : `data = []`.

## timeConfig
**Specify time structure.**

**Type** :
An Object containing 3 properties :
| Name                  | Property                                       | Type       | Default value |
| --------------------- | -----------------------------------------------| ---------- | ------------- |
| **baseUnit**          | time axis' unit                                | *String*   | 'day'         |
| **subUnit**           | time axis' sub unit                            | *String*   | 'hour'        |
| **timeAxisStartRule** | function to calculate time axis' starting date | *Function* | start of week |
| **totalUnit**         | number of base units to display                | *Int*      | 14            |

**Example** :
```shell
timeConfig = {
  baseUnit: 'week',
  subUnit: 'day',
  timeAxisStartRule: function initTimeAxisStartRule(referenceDateDJS) {
    return referenceDateDJS.subtract(1, 'week').startOf('week');
  },
  totalUnits: 8,
}
```
This will display 8 weeks, starting the week before selected/current week.
The subUnit must be thinner than baseUnit and on of this value (hour, day, week, month). It is used to calculate the width of each blocks, and display it under the baseUnits.

**By default** :  `timeConfig = {
  baseUnit: 'day',
  subUnit: 'hour',
  timeAxisStartRule: function initTimeAxisStartRule(referenceDateDJS) {
    return referenceDateDJS.startOf('week');
  },
  totalUnits: 14,
}`.

## styleConfig
**Specify style structure.**

**Type** :
An Object containing 4 properties :
| Name                              | Property                              | Type     | Default value |
| --------------------------------- | ------------------------------------- | -------- | ------------- |
| **firstColumnWidth**              | width of row's first column (px)      | *Int*    | 10            |
| **rowHeight**                     | height of each row - (px)             | *Int*    | 75            |
| **grid**                          | display sub grid in a base unit       | *Boolean*| true          |
| **gridSubdivisions** *(optional)* | number of subdivisions in a base unit | *Int*    | 3             |

**Example** :

```shell
timeConfig = {
  firstColumnWidth: 30,
  rowHeight: 25,
  grid: false,
}
```
This will not display any sub grid to the chart.

**By default** :  `styleConfig = {
  firstColumnWidth: 10,
  rowHeight: 75,
  grid: true,
  gridSubdivisions: 3,
}`.

## blockConfig
**Specify thin blocks' display.**

**Type** :
An Object containing 3 properties :
| Name                | Property                                                                | Type     | Default value |
| ------------------- | ----------------------------------------------------------------------- | -------- | ------------- |
| **baseUnit**        | block's time unit                                                       | *String* | 'hour'        |
| **minUnitsCount**   | number of block's time unit wich determine the limit of too thin blocks | *Int*    | 75            |
| **minDisplayWidth** | fixed minimal width to apply on too thin blocks                         | *Int*    | 20            |

**Example** :

```shell
blockConfig = {
  baseUnit: 'hour',
  minUnitsCount: 8,
  minDisplayWidth: 40,
};
```
A block with a duration strictly below 8 hours will be displayed with a fixed width of 40px.

**By default** :  `blockConfig = {
  baseUnit: 'hour,
  minUnitsCount: 4,
  minDisplayWidth: 20,
}`.