# Get started

## Installation
You can easily add FRISE to your project.

**NPM:**
```shell
npm install --save @fabnumdef/frise
```

**Yarn**:
```shell
yarn add @fabnumdef/frise
```

**Pnpm**:
```shell
pnpm add @fabnumdef/frise
```

## Usage

**Vue2 SFC:**
```html
<template>
  <Frise :data="data" />
</template>

<script>
import Frise from '@fabnumdef/frise';

export default {
  components: {
      Frise
  },
    
  data: () => ({
    data: [
      {
        id: 'RowId#1',
        header: {
          name: 'A380',
          type: 'Airplane'
        },
        blocks: [
          {
            id: 'BlockId#1',
            start: new Date(09/09/2021),
            end: new Date(09/10/2021),
          },
          {
            id: 'BlockId#2',
            start: new Date(09/12/2021),
            end: new Date(09/14/2021),
          },
        ],
      },
    ]
  }),
}    
</script>
```

